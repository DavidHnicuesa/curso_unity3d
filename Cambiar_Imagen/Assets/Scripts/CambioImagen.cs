﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CambioImagen : MonoBehaviour
{
    public Image UIImagen;
    // Start is called before the first frame update
    void Start()
    {
        UIImagen = GameObject.Find("ImageCambiante").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            UIImagen.sprite = Resources.Load<Sprite>("Sprites/Anillas");
        }
        if (Input.GetKeyDown("w"))
        {
            UIImagen.sprite = Resources.Load<Sprite>("Sprites/botella");
        }

    }
}
