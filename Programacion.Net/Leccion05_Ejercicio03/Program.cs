﻿using System;

namespace Leccion05_Ejercicio03
{
    /*Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            int cuatro;
            String linea1;
            String linea2;
            String linea3;
            String linea4;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese segundo valor:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            Console.Write("Ingrese tercero valor:");
            linea3 = Console.ReadLine();
            tres = int.Parse(linea3);
            Console.Write("Ingrese cuarto valor:");
            linea4 = Console.ReadLine();
            cuatro = int.Parse(linea4);
            Console.Write("La suma de los números es:" + (uno + dos + tres + cuatro) + ",  " + "el promedio es:" + ((uno + dos + tres + cuatro)/4));
        }
    }
}
