﻿using System;

namespace Leccion10_Ejercicio01
{
    /*Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la medida de la base y la altura de un triángulo. El programa deberá informar:
a) De cada triángulo la medida de su base, su altura y su superficie.
b) La cantidad de triángulos cuya superficie es mayor a 12.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int medida;
            int altura;
            int veces;
            int contador = 0;
            String linea;
            Console.Write("Ingrese número de triángulos:");
            linea = Console.ReadLine();
            veces = int.Parse(linea);

            for (int i = 1; i <= veces; i++)
            {
                Console.Write("Ingrese la base del triángulo:");
                linea = Console.ReadLine();
                medida = int.Parse(linea);
                Console.Write("Ingrese la altura del triángulo:");
                linea = Console.ReadLine();
                altura = int.Parse(linea);

                Console.WriteLine("La base del triángulo mide: " + medida);
                Console.WriteLine("La altura del triángulo mide: " + altura);
                Console.WriteLine("La superficie del triángulo es: " + (medida*altura));
                if ((medida * altura) >12)
                {
                    contador++;
                }
            }
            Console.WriteLine("Los triángulos con una superficie mayor de 12: " + contador);
        }
    }
}
