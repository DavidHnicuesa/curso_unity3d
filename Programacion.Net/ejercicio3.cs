using System;

public class Ejercicio3 
{
	public static void Main() 
	{
		// Ejercicio 2: Crear una función que genere (devuelva, return) un array 
		//con textos con la info de los NO cercanos:
		// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}
		
		
		
		//Declaro el array de ataques
		float[] ataques = {3.2f,1.7f,2.4f,5.0f,7.1f,4.8f};
		//Declaro el array de ataques válidos o falsos
		bool[] AtacValidos = {false,true,true,false,true,true};
		//Declaro el array de los ataques falsos
		string[] AtaquesFalsos;
		//Inicializo el array para poder meter datos
		AtaquesFalsos = new string[ataques.Length];
		
		
		AtaquesFalsos = EjercicioArrays(ataques, AtacValidos);
	}
	
	public static string EjercicioArrays(float[] ataques, bool[] AtacValidos)
	{		
	string[] AtaquesFalsos;
		//Inicializo el array para poder meter datos
		AtaquesFalsos = new string[ataques.Length];
	
		//Recorro con el for los ataques
		for (int i = 0; i < ataques.Length ; i++)
		{
			//Pregunto si es falso o verdadero
			if  (AtacValidos[i] == false) 
				{
					//Y le añado un 0 si es verdadero
					AtaquesFalsos[i] = "Enemigo " + i + ":" +  ataques[i];
				}
		}
		
		return AtaquesFalsos[];
		
	}
}
