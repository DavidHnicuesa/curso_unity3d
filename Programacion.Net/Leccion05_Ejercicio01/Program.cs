﻿using System;

namespace Leccion05_Ejercicio01
{
    /*Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro del mismo (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro)
     */
    class Program
    {
        static void Main(string[] args)
        {
            int lado;
            int superficie;
            String linea;
            Console.Write("Ingrese el valor del lado del cuadrado:");
            linea = Console.ReadLine();
            lado = int.Parse(linea);
            superficie = lado * 4;
            Console.Write("El perímetro del cuadrado es:");
            Console.Write(superficie);
            Console.ReadKey();
        }
    }
}
