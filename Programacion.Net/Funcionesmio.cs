using System;

public class Funciones 
{
	public static void Main() 
	{
		// Para usar una función, ponemos el nombre y entre paréntesis los argumentos (param)
		double y;
		string valorUsuario; 
		double valUsu; 
		Console.WriteLine("Introduzca valor de X: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		y = FuncionLineal2x3(valUsu);
		Console.WriteLine("Resultado 2*X + 3: " + y);
		
		double z;
		Console.WriteLine("Introduzca valor de X: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		z = FuncionLinealXAlCuadrado(valUsu);
		Console.WriteLine("Resultado X*X+1: " + z);
	}
	// Forma de una función estática
	//   <Modif acceso> static <tipo dato result> <nombre funcion> (<tipo> parám1, <tipo> param2...)
	// y luego entre llaves el cuerpo de la función
	// Con return podemos devolver un valor
	private static double FuncionLineal2x3 (double x) 
	{
		return x * 2 + 3;
	}
	
	private static double FuncionLinealXAlCuadrado (double x) 
	{
		return x * x + 1;
	}
	
	
}





