﻿using System;

namespace Ejemplos_Clases
{
    class Program
    { 
        static void Main()
        {
            Progresion nuevaSerie = new Progresion();
            nuevaSerie.CargarDatos();
            nuevaSerie.GenerarSerie();
            nuevaSerie.NosLoCargamos(7, 3);

            nuevaSerie.NosLoCargamos(13, 9);


            nuevaSerie.x = 5;
            nuevaSerie.y = 7;
            nuevaSerie.GenerarSerie();
        }

        // Este va 25 veces, de 11 en 11 a partir del 11.
        static void MainPorFuncion(string[] args)
        {

            Console.WriteLine("25 veces 11");
            // for (int contador = 1; contador <= 25; contador++)
            //    Console.Write(contador * 11 + " - ");
            MostrarProgresion(25, 11);
            
            MostrarProgresion(13, 9);

            Console.WriteLine("\n7 veces 3");
                MostrarProgresion(7, 3);
            //for (int contador = 1; contador <= 7; contador++)
            //    Console.Write(contador * 3 + " - ");

            Console.WriteLine("\n13 veces 9");
            MostrarProgresion(13, 9);
            //for (int contador = 1; contador <= 13; contador++)
            //    Console.Write(contador * 9 + " - ");

            Console.WriteLine("\nIntroduzca repetición y veces:");
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            MostrarProgresion(x, y);
            
            //for (int contador = 1; contador <= x; contador++)
            //    Console.Write(contador * y + " - ");
        }
        // Ahora que vaya X veces, de Y en Y, a partir de Y. 
        // Donde X, e Y, pueden ser muchos tipos. Por ejemplo. 
        // Haz para el X= 7, Y = 3.
        //  X= 13, Y = 9
        // Para X= lo que el usuario meta, e Y también
        // Imaginaros otros 20 casos

        // Mejora 1: Programación funcional: Con una función, en C#, una función "pura" es un MÉTODO ESTÁTICO
        public static void MostrarProgresion(int x, int y)
        {
            Console.WriteLine(x + "\n veces " + y);
            for (int contador = 1; contador <= x; contador++)
                Console.Write(contador * y + " - ");
        }
    }
}
