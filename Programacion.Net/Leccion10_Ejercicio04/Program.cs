﻿using System;

namespace Leccion10_Ejercicio04
{
    /*Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 12 términos)
Ejemplo: Si ingreso 3 deberá aparecer en pantalla los valores 3, 6, 9, hasta el 36.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            String linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if(num>0 && num<=10)
            {
                for (int i = 1; i <= 12; i++)
                    Console.WriteLine(num + " x " + i + " = " + i * num);
            }else
            {
                Console.Write("El valor es mayor de 10 o menor de 1.");
            }
        }
    }
}
