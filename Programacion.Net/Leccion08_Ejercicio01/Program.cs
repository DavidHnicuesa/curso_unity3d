﻿using System;

namespace Leccion08_Ejercicio01
{
    /*Realizar un programa que pida cargar una fecha cualquiera, luego verificar si dicha fecha corresponde a Navidad.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int dia, mes, año;
            string linea;
            Console.Write("Ingrese un día:");
            linea = Console.ReadLine();
            dia = int.Parse(linea);
            Console.Write("Ingrese un mes:");
            linea = Console.ReadLine();
            mes = int.Parse(linea);
            Console.Write("Ingrese un año:");
            linea = Console.ReadLine();
            año = int.Parse(linea);
            if(dia == 25 && mes == 12)
            {
                Console.Write("¡Es Navidad¡");
                
            }else
            {
                Console.WriteLine(dia + "/" + mes + "/" + año);
                Console.Write("¡No es Navidad¡");
            }
        }
    }
}
