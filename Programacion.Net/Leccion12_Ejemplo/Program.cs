﻿using System;
using System.Collections.Generic;

namespace Leccion12_Ejemplo
{
    class Program
    {
        // Ahora hacemos nombreEn.. como variable GLOBAL:
        // Significa que SÓLO hay UNA por Aplicación.
        // En C# se les llama variables estáticas.
        static string[] nombresEnemigos =
        {
            "Lata 1",           // Objetos de la escena precargados
            "Lata 22 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
            "   Lata 2 (Clone)   ",
            "Lata 2 (CLONE)",
            "   Lata 2   " ,
            "Basura roja 11",
            "Basura roja 1 (Clone)",
            "Basura 32",
            "Basura 3"
        };

        // Ahora lo que hace es leer el array y meter en dos listas los strings que 
        // contienen Clone  y los que no, usando  Contains()
        // Ejercicio 1: Similar, meter en dos listas los de 1 y los de 2 usando IndexOf()
        // Ejercicio 2: Que los que sean de TIPO 2, los meta en otra lista
        // Ejercicio 3: Que los que sean de TIPO 3, los meta en una tercera lista
        // Ejercicio 4: Hacer una función para ello, bool SaberSiEsTipo(String num)
        //              Que las 3 formas usen la esta MISMA FUNCION. Es decir, no repetir el código 3 veces, sólo una
        static void Main(string[] args)
        {

            //string dosPrimeros = "Los dos primeros enemigos son " + nombresEnemigos[0] + ", " + nombresEnemigos[1];
            //Console.WriteLine(dosPrimeros);

            SepararPorClonados();
            SepararPorNumero();


           /* Console.WriteLine("Hola que pasa".IndexOf("Nada"));
            Console.WriteLine("Hola que pasa".IndexOf("Hola"));
            Console.WriteLine("Hola que pasa".IndexOf("que"));
            Console.WriteLine(String.IsNullOrWhiteSpace("Hola que pasa"));*/
        }
        static void SepararPorClonados()
        {
            List<string> objPre = new List<string>();
            List<string> objClo = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (enemigoActual.ToLower().Contains("(clone"))
                {
                    objClo.Add(enemigoActual);
                }
                else
                {
                    objPre.Add(enemigoActual);
                }
            }
            Console.WriteLine("Clonados: " + objClo.Count);
            for (int i = 0; i < objClo.Count; i++)
            {
                string enemigoActual = objClo[i];
                Console.WriteLine("Clonado: " + enemigoActual);
            }
            Console.WriteLine("Precargados: " + objPre.Count);
            foreach (string enemAc in objPre)
            {
                Console.WriteLine("Precargado: " + enemAc);
            }
        }
        static void SepararPorNumero()
        {
            List<string> obj_1 = new List<string>();
            List<string> obj_2 = new List<string>();
            List<string> obj_3 = new List<string>();
            List<string> obj_resto = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (SaberSiEsTipo(enemigoActual, "1"))
                {
                    obj_1.Add(enemigoActual);
                }
                else if (SaberSiEsTipo(enemigoActual, "2"))
                {
                    obj_2.Add(enemigoActual);
                }
                else if (SaberSiEsTipo(enemigoActual, "3"))
                {
                    obj_3.Add(enemigoActual);
                }
                else
                {
                    obj_resto.Add(enemigoActual);
                }

                // Para el ejercicio 4: el if (... IndexOf)
                // Que sea  if (SaberSiEsTipo("1") {  obj_1.Add(enemigoActual); }

                // Si contiene un UNO, puede ser...
               /* if (enemigoActual.IndexOf("1") >= 0)
                {
                    // Cojemos la posición del 1
                    int posChar_1 = enemigoActual.IndexOf("1");
                    // Si el anterior al 1 es un espacio...
                    if (enemigoActual.Substring(posChar_1 - 1, 1) == " ")
                    {
                        // Entonces si es el último caracter
                        if (posChar_1 == enemigoActual.Length - 1)
                        {
                            // Bien! Es del tipo 1
                            obj_1.Add(enemigoActual);
                        }
                        else // Pero puede ser que no se el último 
                        {
                            // Entonces cogemos el siguiente caracter
                            string sigChar = enemigoActual.Substring(posChar_1 + 1, 1);
                            // Y comprobamos que NO sea un número
                            int numDelChar;
                            if (!int.TryParse(sigChar, out numDelChar))
                            {
                                // Si consigue hacer el parseo, es número.
                                // Al negarlo, es que NO ES NÚMERO: 
                                obj_1.Add(enemigoActual);
                            }
                        }
                    }
                }
                else if (enemigoActual.IndexOf("2") >= 0)
                {
                    obj_2.Add(enemigoActual);
                }*/
            }
            Console.WriteLine("TIPOS 1: " + obj_1.Count);
            for (int i = 0; i < obj_1.Count; i++)
            {
                string enemigoActual = obj_1[i];
                Console.WriteLine("Tipo 1: " + enemigoActual);
            }
            Console.WriteLine("TIPOS 2: " + obj_2.Count);
            foreach (string enemAc in obj_2)
            {
                Console.WriteLine("Tipo 2: " + enemAc);
            }
            Console.WriteLine("TIPOS 3: " + obj_3.Count);
            foreach (string enemAc in obj_3)
            {
                Console.WriteLine("Tipo 3: " + enemAc);
            }
            Console.WriteLine("TIPOS: " + obj_resto.Count);
            foreach (string enemAc in obj_resto)
            {
                Console.WriteLine("Tipos: " + enemAc);
            }
        }
        static bool SaberSiEsTipo(String enemigoActual, String num)
        {
            if (enemigoActual.IndexOf(num) >= 0)
            {
                int posChar = enemigoActual.IndexOf(num);
                if (enemigoActual.Substring(posChar - 1, 1) == " ")
                {
                    if (posChar == enemigoActual.Length - 1)
                    {
                        return true;
                    }
                    else
                    {
                        string sigChar = enemigoActual.Substring(posChar + 1, 1);
                        if (!int.TryParse(sigChar, out int numDelChar))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        
        

    }
}




