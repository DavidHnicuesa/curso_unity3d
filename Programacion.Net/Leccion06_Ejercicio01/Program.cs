﻿using System;

namespace Leccion06_Ejercicio01
{
    /*Realizar un programa que lea por teclado dos números, si el primero es mayor al segundo informar su suma y diferencia, en caso contrario informar el producto y la división del primero respecto al segundo.
     */
    class Program
    {
        static void Main(string[] args)
        {
            float uno;
            float dos;
            String linea1;
            String linea2;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = float.Parse(linea1);
            Console.Write("Ingrese primer valor:");
            linea2 = Console.ReadLine();
            dos = float.Parse(linea2);
            if (uno > dos)
            {
                Console.Write("La suma es:" + (uno + dos) + ", " + " Y la diferencia es:" + (uno - dos));
            } else
            {
                Console.Write("El producto es:" + (uno * dos) + ", " + " Y la división es:" + (uno / dos));
            }
        }
    }
}
