﻿using System;

namespace Leccion08_Ejercicio_04
{
    /*Se ingresan por teclado tres números, si al menos uno de los valores ingresados es menor a 10, imprimir en pantalla la leyenda "Alguno de los números es menor a diez".
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            String linea1;
            String linea2;
            String linea3;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese segundo valor:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            Console.Write("Ingrese tercero valor:");
            linea3 = Console.ReadLine();
            tres = int.Parse(linea3);
            if(uno < 10 || dos < 10 || tres < 10)
            {
                Console.Write("Alguno de los números es menor a diez");
            }
        }
    }
}
