﻿using System;

namespace Leccion09_Ejercicio05a
{
    class Program
    {
        /*Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc.
         */
        static void Main(string[] args)
        {
            int valor = 0;
            bool fin = true;

            while (fin)
            {
                Console.WriteLine(valor);

                if (valor > 490 && valor < 500)
                {
                    fin = false;
                }

                valor = valor + 8;
            }
        }
    }
}
