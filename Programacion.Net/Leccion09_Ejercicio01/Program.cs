﻿using System;

namespace Leccion09_Ejercicio01
{
    class Program
    {
        /*Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores.
         */
        static void Main(string[] args)
        {
            int nota;
            int contadorMayor = 0;
            String linea;
            int contadorMenor = 0;
            int contador = 0;
            while (contador < 10)
            {
                Console.Write("Ingrese nota:");
                linea = Console.ReadLine();
                nota = int.Parse(linea);
                contador = contador + 1;
                if (nota >= 7)
                {
                    contadorMayor = contadorMayor + 1;
                }
                else
                {
                    contadorMenor = contadorMenor + 1;
                }
            }
            Console.WriteLine("Notas mayores o iguales a 7:" + contadorMayor);
            Console.Write("Notas menores a 7:" + contadorMenor);

        }
    }
}
