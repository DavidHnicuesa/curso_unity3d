﻿using System;

namespace Leccion10_Ejercicio03
{
    /*Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50)
     */
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 10; i++)
                Console.WriteLine("5 x " + i + " = " + i * 5);
            
        }
    }
}
