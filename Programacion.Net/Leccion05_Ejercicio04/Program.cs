﻿using System;

namespace Leccion05_Ejercicio04
{
    /*Se debe desarrollar un programa que pida el ingreso del precio de un artículo y la cantidad que lleva el cliente. Mostrar lo que debe abonar el comprador.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            String linea1;
            String linea2;
            Console.Write("Ingrese el precio del producto:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese la cantidad de productos comprados:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            Console.Write("El importe total es:");
            Console.Write(uno * dos);
        }
    }
}
