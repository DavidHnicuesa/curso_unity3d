﻿using System;

namespace Leccion08_Ejercicio02
{
    /*Se ingresan tres valores por teclado, si todos son iguales se imprime la suma del primero con el segundo y a este resultado se lo multiplica por el tercero.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            String linea1;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese segundo valor:");
            linea1 = Console.ReadLine();
            dos = int.Parse(linea1);
            Console.Write("Ingrese tercero valor:");
            linea1 = Console.ReadLine();
            tres = int.Parse(linea1);
            if(uno == dos && dos== tres)
            {
                Console.Write("La suma:" + (uno + dos) + " más el producto:" + (uno + dos) * tres );
            }else
            {
                Console.Write("No son todos iguales.");
            }
        }
    }
}
