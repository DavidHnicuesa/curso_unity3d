﻿using System;

namespace Leccion07_Ejercicio03
{
    /*Confeccionar un programa que permita cargar un número entero positivo de hasta tres cifras y muestre un mensaje indicando si tiene 1, 2, o 3 cifras. Mostrar un mensaje de error si el número de cifras es mayor.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            String Linea1;

            Console.WriteLine("Introducir un número menor de tres cifras:");
            Linea1 = Console.ReadLine();
            uno = int.Parse(Linea1);
            if(uno > 0 && uno < 10)
            {
                Console.Write("El número tiene una cifra.");
            }else
                {
                if(uno > 9 && uno < 100)
                {
                    Console.Write("El número tiene dos cifras.");
                }else
                {
                    if(uno > 99 && uno < 1000)
                    {
                        Console.Write("El número tiene tres cifras.");
                    }else
                    {
                        if(uno > 999)
                        {
                            Console.Write("Error: El número tiene más de tres cifras.");

                        }
                    }
                }
            }
        }
    }
}
