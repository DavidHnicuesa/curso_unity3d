﻿using System;

namespace Leccion09_Ejercicio07
{
    /*Desarrollar un programa que permita cargar n números enteros y luego nos informe cuántos valores fueron pares y cuántos impares.
Emplear el operador “%” en la condición de la estructura condicional:
	if (valor%2==0)         //Si el if da verdadero luego es par.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int par=0;
            int impar=0;
            int num;
            String linea;
            bool continuar = true;
            while (continuar)
            {
                Console.Write("Ingrese un número (para salir escribe q):");
                linea = Console.ReadLine();
                if(linea == "q")
                {
                    continuar = false;
                }
                else
                {
                    num = int.Parse(linea);
               
                    if(num%2==0)
                    {
                        par++;
                    }else
                    {
                        impar++;
                    }
                }
            }
                Console.WriteLine("Numeros pares: " + par);
                Console.WriteLine("Numeros impares: " + impar);
        }
    }
}
