﻿using System;

namespace Leccion06_Ejercicio02
{
    /*Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado".
     */
    class Program
    {
        static void Main(string[] args)
        {
            float uno;
            float dos;
            float tres;
            String linea1;
            String linea2;
            String linea3;
            Console.Write("Ingrese primera nota:");
            linea1 = Console.ReadLine();
            uno = float.Parse(linea1);
            Console.Write("Ingrese primera nota:");
            linea2 = Console.ReadLine();
            dos = float.Parse(linea2);
            Console.Write("Ingrese primera nota:");
            linea3 = Console.ReadLine();
            tres = float.Parse(linea3);
            if ((uno + dos + tres) / 3 >= 7)
                Console.Write("Promocionado");
        }
    }
}
