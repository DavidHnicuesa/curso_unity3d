﻿using System;

namespace Leccion08_Ejercicio07
{
    /*Escribir un programa en el cual: dada una lista de tres valores numéricos distintos se calcule e informe su rango de variación (debe mostrar el mayor y el menor de ellos)
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            int mayor = 0;
            int menor = 0;
            String linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            uno = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            dos = int.Parse(linea);
            Console.Write("Ingrese tercero valor:");
            linea = Console.ReadLine();
            tres = int.Parse(linea);

            // Uno Mayor, tres menor, dos menor
            if (uno > dos && uno > tres)
            {
                Console.WriteLine("El mayor es el primero: " + uno);
                mayor = uno;
            }else if (dos > tres)
            {
                Console.WriteLine("El menor es el tercero: " + tres);
                menor = tres;
            }
            else
            {
                Console.WriteLine("El menor es el segundo: " + dos);
                menor = dos;
            }

            // dos Mayor, tres menor,  uno menor
            if (dos > uno && dos > tres)
            {
                Console.WriteLine("El mayor es el segundo: " + dos);
                mayor = dos;
            }else if(uno > tres) 
                {
                Console.WriteLine("El menor es el tercero: " + tres);
                menor = tres;
            }
            
            // tres Mayor, tres menor,  uno menor
            if (tres > uno && tres > dos)
            {
                Console.WriteLine("El mayor es el tercero: " + tres);
                mayor = tres;
            }

            Console.WriteLine("El rango de variación es: " + (mayor - menor));
                
        }
    }
}
