﻿using System;

namespace Leccion08_Ejercicio05
{
    class Program
    {
        /*Escribir un programa que pida ingresar la coordenada de un punto en el plano, es decir dos valores enteros x e y (distintos a cero).
Posteriormente imprimir en pantalla en que cuadrante se ubica dicho punto. (1º Cuadrante si x > 0 Y y > 0 , 2º Cuadrante: x < 0 Y y > 0, etc.)
        */
        static void Main(string[] args)
        {
            int x;
            int y;
            String linea;
            Console.Write("Ingrese coordenada X:");
            linea = Console.ReadLine();
            x = int.Parse(linea);
            Console.Write("Ingrese coordenada Y:");
            linea = Console.ReadLine();
            y = int.Parse(linea);
            if(x != 0 && y != 0)
            {
                if(x > 0 && y > 0)
                {
                    Console.Write("1º Cuadrante");
                }
                if (x < 0 && y < 0)
                {
                    Console.Write("2º Cuadrante");
                }
            }
        }
    }
}
