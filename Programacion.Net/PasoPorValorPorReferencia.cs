// Al principio se ponen las importaciones
using System;

public class PasoPorvalorPorReferencia
{
    public static void Main()
    {

        try
        {
            Console.WriteLine("Paso por valor:");
            int y = 10;
            CambiarVariablePorValor(y);
            Console.WriteLine("Y = a " + y);

            CambiarVariablePorReferencia(out y);
            Console.WriteLine("Y = a " + y);

            string supuestoNumero_1 = "200", supuestoNumero_2 = "LOO";
            int numero_1 = Int32.Parse(supuestoNumero_1);
            int numero_2 = Int32.Parse(supuestoNumero_2);
            Console.WriteLine("Num 1 = " + numero_1 + ", Num 2 = " + numero_2);
        }

        catch (FormatException error)
        {
            Console.WriteLine("Error de formato");
            Console.WriteLine(error.Message);
        }
        catch (IndexOutOfRangeException error)
        {
            Console.WriteLine("Error de limite de Array");
            Console.WriteLine(error.Message);
        }
        catch (Exception error)
        {
            Console.WriteLine("Error gen�rico");
            Console.WriteLine(error.Message);
        }  

 //Ejemplo de paso por referencia
    if (Int32.TryParse(supuestoNumero_1, out numero_1))
        {
            Console.WriteLine("OK, Num 1 = " + numero_1);
        }else
            Console.WriteLine("Casca en el formateo");
        {
    if (Int32.TryParse(supuestoNumero_2, ref numero_1))
        {
                Console.WriteLine("OK, Num 2 = " + numero_2);
            }
            else
                Console.WriteLine("Casca en el formateo");
            {
            }

   
        }
    static void CambiarVariablePorValor(int x)
    {
        x = 20;
        
    }
    static void CambiarVariablePorReferencia(out int z)
    {
        z = 20;

    }
}