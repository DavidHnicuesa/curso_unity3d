﻿using System;

namespace Leccion07_Ejercicio04
{
    /*Un postulante a un empleo, realiza un test de capacitación, se obtuvo la siguiente información: cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contestó correctamente. Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo según el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:
	Nivel máximo:	Porcentaje>=90%.
	Nivel medio:	Porcentaje>=75% y <90%.
	Nivel regular:	Porcentaje>=50% y <75%.
	Fuera de nivel:	Porcentaje<50%.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            String linea1;
            String linea2;
            float porcentaje;

            Console.Write("Ingrese números de preguntas:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese número de respuestas correctas:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            porcentaje = ((dos * 100) /uno );
            Console.Write(porcentaje);
            if (porcentaje >= 90)
            {
                Console.Write("Nivel máximo:	Porcentaje>=90%.");
            }
            else
            {
                if(porcentaje >= 75 && porcentaje < 90)
                {
                    Console.Write("Nivel medio:	Porcentaje>=75% y <90%.");
                }else
                {
                    if(porcentaje >= 50 && porcentaje < 75)
                    {
                        Console.Write("Nivel regular:	Porcentaje>=50% y <75%.");
                    }else
                    {
                        if(porcentaje < 50)
                        {
                            Console.Write("Fuera de nivel:	Porcentaje<50%.");
                        }
                    }
                }
            }


        }
    }
}
