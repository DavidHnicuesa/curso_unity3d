﻿using System;

namespace Leccion10_Ejercicio05
{
    /*Realizar un programa que lea los lados de n triángulos, e informar:
a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales), o escaleno (ningún lado igual)
b) Cantidad de triángulos de cada tipo.
c) Tipo de triángulo que posee menor cantidad.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int num, lado1, lado2, lado3;
            int cont1 = 0, cont2 = 0, cont3 = 0;
            String linea;

            Console.Write("Ingrese cuántos triángulos quiere ver:");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            for(int i=1; i <= num; i++)
            {
                Console.Write("Ingrese primer lado:");
                linea = Console.ReadLine();
                lado1 = int.Parse(linea);
                Console.Write("Ingrese primer lado:");
                linea = Console.ReadLine();
                lado2 = int.Parse(linea);
                Console.Write("Ingrese primer lado:");
                linea = Console.ReadLine();
                lado3 = int.Parse(linea);
                if (lado1 == lado2 && lado2 == lado3)
                {
                    Console.WriteLine("El triángulo es equilátero.");
                    cont1++;
                }else if(lado1 == lado2 || lado2 == lado3|| lado1 == lado3)
                {
                    Console.WriteLine("El triángulo es isósceles.");
                    cont2++;
                }
                else
                {
                    Console.WriteLine("El triángulo es escaleno.");
                    cont3++;
                }
            }
            Console.WriteLine("Equiláteros: " + cont1 + ". Isósceles: " + cont2 + ". Escaleno: " + cont3);
            if (cont1 < cont2 && cont1 < cont3)
            {
                Console.WriteLine("Hay más equiláteros.");
            }
            else if (cont2 < cont3)
            {
                Console.WriteLine("Hay más isósceles.");
            }
            else
            {
                Console.WriteLine("Hay menos escaleno.");
            }
        }
    }
}
