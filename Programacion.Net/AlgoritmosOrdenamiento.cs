﻿using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();
			numeros.Add(4);
			numeros.Add(5);
			numeros.Add(3);
			numeros.Add(1);
			numeros.Add(2);
	
		List<int> numOrder = new List<int>();

		MostrarLista(numeros);
		
		for (int i = numeros.Count; i > 0; i--)
        {
			Menor(numeros, numOrder);
        }

		MostrarLista(numeros);
		MostrarLista(numOrder);
	}

	static void MostrarLista(List<int> lista)
    {		
		Console.WriteLine("Lista: ");
		for (int i = 0; i < lista.Count; i++)
        {
			Console.Write(" : " + lista[i] + ", ");
        }
		Console.WriteLine(" FIN");	
     }

	public static void Menor(List<int> lista, List<int> orden)
	{
		int menor = lista[0];
		int indice = 0;
		for (int i = 0; i < lista.Count; i++)
		{
			if (lista[i] < menor)
			{
				menor = lista[i];
				indice = i;
			}
		}
		orden.Add(lista[indice]);
		lista.RemoveAt(indice);
	}
	
}
