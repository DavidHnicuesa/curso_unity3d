using System;

public class Ejercicio3 
{
	//Ejercicio 4: Otra que devuelva el ataque máximo del array	(resultado: 7.1 es máx)

public static void Main() 
	{
		//Declaro el array y lo cargo
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};

		//Declaro la variable para mostrar el máximo
		float numMax = ataques[0];

		//recorro el array para buscar el máximo
		for (int i = 0; i < ataques.Length; i++) 
			{
				//escribo el valor del array en este momento
				Console.WriteLine("Valor de " + i + " es " + ataques[i]);
				//comparo cada elemento con el número a mostrar máximo
				if (ataques[i]> numMax) 
				{
					//si es mayor lo introduzco en la variable para poder compararlo
					numMax =  ataques[i];
				}
			}
		//Muestro el número máximo
		Console.WriteLine("####################");	
		Console.WriteLine("Valor Máximo es " + numMax);	
	}
}