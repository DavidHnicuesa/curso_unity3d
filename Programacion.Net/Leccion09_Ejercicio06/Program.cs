﻿using System;

namespace Leccion09_Ejercicio06
{
    /*Realizar un programa que permita cargar dos listas de 5 valores cada una. Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
Tener en cuenta que puede haber dos o más estructuras repetitivas en un algoritmo.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int contador = 0;
            String linea;
            int valor;
            
            int total1 = 0;
            int total2 = 0;
            Console.Write("Ingrese la primera lista:");
            while (contador < 5)
            {
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                total1 = total1 + valor;
                contador = contador + 1;
            }
            contador = 0;
            Console.Write("Ingrese la segunda lista:");
            while (contador < 5)
            {
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                total2 = total2 + valor;
                contador = contador + 1;
            }
            if(total1 > total2)
            {
                Console.Write("La lista primera es mayor.");
            }else if (total1 < total2)
            {
                Console.Write("La lista segunda es mayor.");
            }else
            {
                Console.Write("Las listas son iguales.");
            }
            /*operador---
            Console.WriteLine(total1 > total2 ? "mayor1" : total1 < total2 ? "mayor2" : "Son iguales");
            */
        }
    }
}
