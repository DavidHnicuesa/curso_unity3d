﻿using System;

namespace Leccion08_Ejercicio06
{
    /*De un operario se conoce su sueldo y los años de antigüedad. Se pide confeccionar un programa que lea los datos de entrada e informe:
a) Si el sueldo es inferior a 500 y su antigüedad es igual o superior a 10 años, otorgarle un aumento del 20 %, mostrar el sueldo a pagar.
b)Si el sueldo es inferior a 500 pero su antigüedad es menor a 10 años, otorgarle un aumento de 5 %.
c) Si el sueldo es mayor o igual a 500 mostrar el sueldo en pantalla sin cambios.
    */
    class Program
    {
        static void Main(string[] args)
        {
            float sueldo;
            float anos;
            String linea;
            Console.Write("Ingrese sueldo:");
            linea = Console.ReadLine();
            sueldo = float.Parse(linea);
            Console.Write("Ingrese años de antigüedad:");
            linea = Console.ReadLine();
            anos = float.Parse(linea);
            if(sueldo < 500 && anos >= 10)
            {
                Console.Write("Su nuevo sueldo es:" + (sueldo * 1.2));
            }
            if (sueldo <= 500 && anos < 10)
            {
                Console.Write("Su nuevo mísero sueldo es:" + (sueldo * 1.05));
            }
            if (sueldo > 500)
            {
                Console.Write("Su sueldo es:" + sueldo );
            }
        }
    }
}
