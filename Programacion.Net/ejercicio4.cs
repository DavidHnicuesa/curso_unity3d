using System;

public class Ejercicio3 
{
	
	// Ejercicio 2: Generar un array con textos con la info de los NO cercanos:
	// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}
	
	public static void Main() 
	{
		EjercicioArrays();
	}
	
	public static void EjercicioArrays()
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos = {false, true, true, false, true, true};
		
		float[] totalAtaques(6);
		totalAtaques = CalcAtaqueTotalCercanos( ataques, siCercanos );
				
		
		
		Console.WriteLine("Total = " + totalAtaques);
		
		public static float CalcAtaqueTotalCercanos(float[] ataques, bool[] siCercanos)
	{		
		float[] total;
		
		total = 0;	// Inicializar variables!!
		
		// Para cuando los ataques sumen sean los verdaderos
		for (int i = 0; i < ataques.Length; i = i + 1) 
		{
			Console.WriteLine("Valor de " + i + " es " + ataques[i]);
			if (siCercanos[i]) {
				total = total + ataques[i];
			}
		}
		return total;
		
	}


	}

}