﻿using System;

namespace Leccion06_Ejercicio03
{
    /*Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) mostrar un mensaje indicando si el número tiene uno o dos dígitos.
(Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero)
    */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            String linea1;
            Console.Write("Ingrese un valor positivo de 0 a 99:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            if(uno > 9)
            {
                Console.Write("El número tiene dos cifras.");
            }else
            {
                Console.Write("El número tiene una cifra.");
            }
        }
    }
}
