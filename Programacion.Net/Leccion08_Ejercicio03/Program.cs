﻿using System;

namespace Leccion08_Ejercicio03
{
    /*Se ingresan por teclado tres números, si todos los valores ingresados son menores a 10, imprimir en pantalla la leyenda "Todos los números son menores a diez".
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            String linea1;
            String linea2;
            String linea3;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese segundo valor:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            Console.Write("Ingrese tercero valor:");
            linea3 = Console.ReadLine();
            tres = int.Parse(linea3);
            if(uno < 10 && dos < 10 && tres < 10)
            {
                Console.Write("Todos los números son menores de diez.");
            }
        }
    }
}
