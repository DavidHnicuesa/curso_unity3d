﻿using System;

namespace Leccion10_Ejercicio02
{
    /*Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos 5 valores ingresados.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            int valor = 0;
            String linea;

            for (int i = 1; i <= 10; i++)
            {
                Console.Write("Ingrese primer valor:");
                linea = Console.ReadLine();
                num = int.Parse(linea);
                if (i > 5)
                {
                    valor = valor + num;
                }
            }
                  Console.WriteLine(valor);
        }
    }
}
