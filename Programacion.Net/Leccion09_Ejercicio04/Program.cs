﻿using System;

namespace Leccion09_Ejercicio04
{
    class Program
    {
        /*Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado)
         */
        static void Main(string[] args)
        {
            int contador = 0;
            int num = 11;
            while (contador < 25)
            {
                Console.WriteLine(num);
                num = num + 11;
                contador = contador + 1;
            }
        }
    }
}
