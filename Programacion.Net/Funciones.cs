using System;

public class Funciones 
{
	public static void Main() 
	{
		// Para usar una función, ponemos el nombre y entre paréntesis los argumentos (param)
		double y;
		string strValUsu; 
		double valUsu; 
		Console.WriteLine("Introduzca valor de X: ");
		strValUsu = Console.ReadLine();
		valUsu = Double.Parse(strValUsu);
		y = FuncionLineal2x3(valUsu);
		Console.WriteLine("Resultado 2*X + 3: " + y);
		
		
		Console.WriteLine("Resultado X^2 + 1: " 
			+ FuncionXel2_1(Float.Parse(strValUsu)));
		
	}
	// Forma de una función estática
	//   <Modif acceso> static <tipo dato result> <nombre funcion> (<tipo> parám1, <tipo> param2...)
	// y luego entre llaves el cuerpo de la función
	// Con return podemos devolver un valor
	private static double FuncionLineal2x3 (double x) 
	{
		return x * 2 + 3;
	}
	
	static float FuncionXel2_1(float param) {
		float resultado = param * param + 1;
		return resultado;
	}

}





