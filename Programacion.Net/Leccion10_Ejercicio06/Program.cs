﻿using System;

namespace Leccion10_Ejercicio06
{
    /*Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
    */
    class Program
    {
        static void Main(string[] args)
        {
            int x = 0;
            int y = 0;
            int num = 0;
            int cont1 = 0, cont2 = 0, cont3 = 0, cont4 = 0;
            String linea, linea2;
            Console.Write("Ingrese número de veces:");
            linea = Console.ReadLine();
            num = int.Parse(linea);
           
            for (int i = 0; i <num; i++)
                {
                Console.Write("Ingrese coordenadas(x,y):");
                linea = Console.ReadLine();
                x = int.Parse(linea);
                linea2 = Console.ReadLine();
                y = int.Parse(linea2);

                    if (x != 0 && y != 0)
                    {
                        if (x >= 0 && y >= 0)
                        {
                            cont1++;
                        }else if (x >= 0 && y <= 0)
                        {
                            cont2++;
                        }else if (x <= 0 && y >= 0)
                        {
                            cont3++;
                        }
                        else
                        {
                            cont4++;
                        }
                }else
                {
                    Console.WriteLine("Has introducido 0,0");
                }
            }
            Console.Write("Has introducido " + cont1 + " primer cuadrante. " + cont2 + " segundo cuadrante. " + cont3 + " tercer cuadrante. " + cont4 + " cuarto cuadrante.");

        }
    }
}
