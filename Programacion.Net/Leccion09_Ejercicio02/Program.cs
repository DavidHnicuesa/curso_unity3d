﻿using System;

namespace Leccion09_Ejercicio02
{
    /*Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int altura = 1;
            int total = 0;
            String linea;
            bool continuar = true;
            int contador = 0;
            while (continuar)
            {
                Console.Write("Ingrese altura (si quiere parar introduce 0):");
                linea = Console.ReadLine();
                altura = int.Parse(linea);
               contador = contador + 1;
                if (altura == 0)
                {
                    continuar = false;
                    contador = contador - 1;
                }
                total = total + altura;
                
            }
            Console.Write("La altura media es:" + total / contador);
        }
    }
}
