﻿using System;

namespace Leccion07_Ejercicio02
{
    /*Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo, nulo o negativo.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            String linea1;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            if (uno > 0)
            {
                Console.Write("El número es positivo");
            }else if (uno < 0)
            {
                Console.Write("El número es negativo");
            }else
            {
                Console.Write("El número es nulo");
            }
               
        }
    }
}
