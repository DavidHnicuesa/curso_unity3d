﻿using System;

namespace Leccion09_ejercicio03
{
    class Program
    {
        /*En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa deberá informar el importe que gasta la empresa en sueldos al personal.
         */
        static void Main(string[] args)
        {
            int sueldo = 0;
            int contadorMenor = 0;
            int contadorMayor = 0;
            String linea;
            bool continuar = true;
            int gastos = 0;
            
            while (continuar)
            {
                Console.Write("Ingrese sueldo (si quiere parar introduce 0):");
                linea = Console.ReadLine();
                sueldo = int.Parse(linea);
                gastos = gastos + sueldo;
                if (sueldo >= 100 && sueldo <= 300)
                    contadorMenor = contadorMenor + 1;
                if (sueldo > 300)
                    contadorMayor = contadorMayor + 1;
                if (sueldo == 0)
                    continuar = false;
            }

                Console.WriteLine("Los empleados cobran entre $100 y $300 son :" + contadorMenor);
                Console.WriteLine("Los empleados cobran más de $300 son :" + contadorMayor);
                Console.WriteLine("Los gastos en sueldos son :" + gastos);
        }
    }
}
