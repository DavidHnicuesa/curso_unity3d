﻿using System;

namespace Leccion07_Ejercicio01
{
    /*Se cargan por teclado tres números distintos. Mostrar por pantalla el mayor de ellos.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int uno;
            int dos;
            int tres;
            String linea1;
            String linea2;
            String linea3;
            Console.Write("Ingrese primer valor:");
            linea1 = Console.ReadLine();
            uno = int.Parse(linea1);
            Console.Write("Ingrese segundo valor:");
            linea2 = Console.ReadLine();
            dos = int.Parse(linea2);
            Console.Write("Ingrese tercero valor:");
            linea3 = Console.ReadLine();
            tres = int.Parse(linea3);

            if ( uno > dos)
            {
                if (uno > tres)
                    Console.Write("El mayor es: " + uno);
            }
            else
            {
                if (dos>tres)
                {
                    Console.Write("El número mayor es: " + dos);
                }else
                {
                    Console.Write("El número mayor es: " + tres);
                }
                    
            }
        }
    }
}
