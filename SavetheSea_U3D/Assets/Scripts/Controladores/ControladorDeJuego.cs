﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

public class ControladorDeJuego: MonoBehaviour
{
    [Header("Vida:")]
    public int vidas = 3;
    [Header("Prefab y obj. juego:")]
    public GameObject[] prefabsEnemigos;

    public GameObject esquinaSuperior;
    [Header("Objetos UI:")]
    public GameObject panelFinJuego;
    public GameObject panelGanador;
    public GameObject panelPerdedor;
    public UnityEngine.UI.Text textoVidas;
    public UnityEngine.UI.Text textoPuntos;
    [Header("Listado de enemigos:")]
    public ApariciónEnemigo[] apariciones;
    
    int enemigoActual;
    float timeInicio;
    int puntos = 0;

    // Use this for initialization
    void Start()
    {
        textoVidas.text = "" + this.vidas;
        textoPuntos.text = "" + this.puntos;
       /* this.apariciones = new ApariciónEnemigo[3];
        this.apariciones[0] = new ApariciónEnemigo(2 , 1);
        this.apariciones[1] = new ApariciónEnemigo(-3 , 6);
        this.apariciones[2] = new ApariciónEnemigo(7 , 10);*/
        enemigoActual = 0;
        timeInicio = Time.time;
       
    }

    // Update is called once per frame
    void Update()
    {
        float tiempoActual = Time.time - timeInicio;
        if (enemigoActual < apariciones.Length)
        {
            if (tiempoActual > apariciones[enemigoActual].tiempoInicio)
            {
                int indiceEnem = apariciones[enemigoActual].IndiceEnemigo;
                GameObject prefabEnemigo = prefabsEnemigos[indiceEnem];

                GameObject enemigo = GameObject.Instantiate(prefabEnemigo);
                float posX = apariciones[enemigoActual].posicionX;
                enemigo.transform.position = new Vector3(posX, esquinaSuperior.transform.position.y, 0);
                enemigo.GetComponent<Enemigo>().controlJuego = this;
                enemigo.GetComponent<Enemigo>().numEnemigo = enemigoActual;
                enemigoActual++;                              
            }
        }       
    }
    public void SumarPuntos(int puntos, int numEnem)
    {
        this.puntos += puntos;
        textoPuntos.text = "" + this.puntos;
        ComprobarFinJuego(numEnem);

    }
    public void RestarVida(int numEnem)
    {
        this.vidas--;
        ComprobarFinJuego(numEnem);
        textoVidas.text = "" + this.vidas;
    }
    private void ComprobarFinJuego(int numEnem)
    {
        if (vidas <= 0)
        {
            panelFinJuego.SetActive(true);
            panelPerdedor.SetActive(true);
            this.enabled = false;
            this.vidas = 0;
        }else if (numEnem == this.apariciones.Length - 1)
        {
            panelFinJuego.SetActive(true);
            panelGanador.SetActive(true);
            this.enabled = false;
        }
    }

}
