﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DavidSTS
{ 
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Propiedades ctrl escenas:")]
        public string nombreEscena;
        
    void Imprimir(int x)
        {
            if (x > 0)
            {
                Debug.Log("Entrando: " + x);
                Imprimir(x - 1);
            }
            Debug.Log("Saliendo: " + x);
        }
        private void Start()
        {
            if (nombreEscena != "")
            {
               // print("Arrancando escena " + nombreEscena);
                this.CargarEscena(nombreEscena);
            }
                AsignarCerrarAlBotonSalir();
            
               
            /*
            GameObject botonSalir = GameObject.Find("Button_Salir");
            botonSalir.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);
            AsignarCerrarAlBotonSalir();
            */
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape)) 
            {
                CerrarAplicacion();
            }
        }

        public void CargarEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }
        public void CerrarAplicacion()
        {
            Application.Quit();
        }

        void AsignarCerrarAlBotonSalir()
        {
            GameObject objCanvas = GameObject.Find("Canvas");
            if (objCanvas != null)
            {
                Transform canvas = GameObject.Find("Canvas").GetComponent<Transform>();
                Transform Boton_Salir_Si = null;
                FindByNameInactives(canvas, "Boton_Salir_Si", ref Boton_Salir_Si);

                if (Boton_Salir_Si != null)
                {
                    Boton_Salir_Si.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);
                }
            }
        }

        void  FindByNameInactives(Transform raiz, string nombre, ref Transform objetoEncontrado)
        {
            //objetoEncontrado = null;
            for (int i=0; i < raiz.childCount; i++)
            {
                Transform objTransf = raiz.GetChild(i);
                if (objTransf.name == nombre)
                {
                    objetoEncontrado = objTransf;
                    return;
                }
                else
                {
                    FindByNameInactives(objTransf, nombre, ref objetoEncontrado);      
                }
            }
           
        }
    }

}
