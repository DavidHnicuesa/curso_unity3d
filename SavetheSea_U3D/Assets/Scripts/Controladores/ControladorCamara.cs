﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCamara : MonoBehaviour
{
    public float rangoLerp = 0.03f;
    private GameObject jugador;
    private Transform LimiteCamaraIzq;
    private Transform limCAmDer;
   

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Jugador_caballito");
        limCAmDer = GameObject.Find("Der_Cam").GetComponent<Transform>();
        LimiteCamaraIzq = GameObject.Find("Izq_Cam").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 posCamara = transform.position;

        float x = Mathf.Lerp(this.transform.position.x, jugador.transform.position.x, rangoLerp);

        posCamara.x = x;

        if (posCamara.x < LimiteCamaraIzq.position.x)
        {
            posCamara.x = LimiteCamaraIzq.position.x;
        }

        if (posCamara.x > limCAmDer.position.x)
        {
            posCamara.x = limCAmDer.position.x;
        }



        this.transform.position = posCamara;
    }
}
