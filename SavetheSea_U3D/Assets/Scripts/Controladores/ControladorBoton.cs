﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorBoton : MonoBehaviour
{
    public int direccion;
    public GameObject jugador;
    private bool pulsando;
    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pulsando)
        {
            jugador.GetComponent<MovimientoJugador>().Mover(direccion);
            if (direccion < 0)
            {
                jugador.GetComponent<AnimacionJugador>().HaciaLaIzquierda();
                
            }
            else if (direccion > 0)
            {
                jugador.GetComponent<AnimacionJugador>().HaciaLaDerecha();
                
            }
        }
      
}
    private void OnMouseDown()
    {
        pulsando = true;
       
    }
    private void OnMouseUp()
    {
        
        pulsando = false;
    }
}
