﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionJugador : MonoBehaviour
{
    public Transform transfSpritesCaballito;
    public Animator animCtrlCuerpo;
    public Animator animCtrlAlas;
    public Animation animCabeza;

    public void HaciaLaDerecha()
    {
        animCtrlCuerpo.SetInteger("direccion", +1);
        animCtrlAlas.speed = 2.5f;
        
        transfSpritesCaballito.localScale = new Vector3(-1, 1, 1);
        animCabeza.CrossFade("Nadar_Cabeza_1");
        //animCtrlAlas.speed = 3f;
    }
    public void HaciaLaIzquierda()
    {
        animCtrlCuerpo.SetInteger("direccion", -1);
        transfSpritesCaballito.localScale = new Vector3(1, 1, 1);
        animCabeza.CrossFade("Nadar_Cabeza_1");
        
        animCtrlAlas.speed = 3f;
    }
    public void Parado()
    {
        animCtrlCuerpo.SetInteger("direccion", 0);
        animCabeza.CrossFade("Idle_Cabeza");
        
        animCtrlAlas.speed = 1;
    }

}
