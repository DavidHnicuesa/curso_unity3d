﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public ControladorDeJuego controlJuego;
    AudioSource audioCogerLata;
    public AudioClip audioChocarSuelo;
    public float velocidad;
    public int numEnemigo;
    GameObject jugador;
   


    void Start()
    {
        jugador = GameObject.Find("Jugador");
        audioCogerLata = GameObject.Find("Coger_Lata").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0)* Time.deltaTime;

        this.GetComponent<Transform>().position += movAbajo;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.enabled)
        {
            //Debug.Log("Enemigo colisionado con " + collision.gameObject.name);
            if (collision.gameObject.name.ToLower().Contains("marino"))
            {
                this.enabled = false;
                this.GetComponent<Animator>().speed = 0;
                controlJuego.RestarVida(this.numEnemigo);
                AudioSource.PlayClipAtPoint(audioChocarSuelo, Vector3.zero);
            }
            if (collision.gameObject.name.ToLower().Contains("cuerpo"))
        {
            audioCogerLata.Play();
            Destroy(this.gameObject);
            controlJuego.SumarPuntos(100,this.numEnemigo);
                
            }

        }
        
    }
}
