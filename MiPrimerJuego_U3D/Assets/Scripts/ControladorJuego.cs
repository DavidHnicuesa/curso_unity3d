﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos;
    public AparicionEnemigo[] aparicionesEnem;

    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    private float timeIni;
    private int enemActual;

    // Start is called before the first frame update
    void Start()
    {
        timeIni = Time.time;
        enemActual = 0;
    }

    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
        textoPuntos.GetComponent<Text>().text = "Puntos: " + this.puntos;
        //Para saber si un enemigo tiene que aparecer, tenemos que saber cuanto tiempo 
        //ha pasado desde el inicio del nivel hasta el momento actual.
        //(frame actual) y si es superior al tiempo configurado en la aparación del enemigo.
        float tiempoActual = Time.time - timeIni;

        if ( tiempoActual > aparicionesEnem[enemActual].tiempoInicio)
        {
            //Si no ha aparecido...
            if ( ! aparicionesEnem[enemActual].yaHaAparecido )
            {
                this.InstanciarEnemigo();
                aparicionesEnem[enemActual].yaHaAparecido = true;
                enemActual++;
            }
            
        }
    }

    // Esto es un método (acción, conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigoLata()
    {
        this.puntos = this.puntos + 10;     // this.vidas++;   
        //InstanciarEnemigo();
    }

    public void CuandoPerdemosEnemigoLata()
    {
        this.vidas = this.vidas - 1;        // this.vidas--;
        //InstanciarEnemigo();
    }

    public void CuandoCapturamosEnemigoBotella()
    {
        this.puntos = this.puntos + 10;     // this.vidas++;

        //InstanciarEnemigo();
    }

    public void CuandoPerdemosEnemigoBotella()
    {
        this.vidas = this.vidas - 1;        // this.vidas--;

        //InstanciarEnemigo();
    }

    public void CuandoCapturamosEnemigoAnillas()
    {
        this.puntos = this.puntos + 10;        // this.vidas--;

        //InstanciarEnemigo();
    }

    public void CuandoPerdemosEnemigoAnillas()
    {
        this.vidas = this.vidas - 1;        // this.vidas--;

        //InstanciarEnemigo();
    }

    public void CuandoCapturamosEnemigoCascara()
    {
        this.puntos = this.puntos + 10;        // this.vidas--;

        //InstanciarEnemigo();
    }

    public void CuandoPerdemosEnemigoCascara()
    {
        this.vidas = this.vidas - 1;        // this.vidas--;

        //InstanciarEnemigo();
    }

    public void InstanciarEnemigo()
    {
        // Asigno a la variable un número random entre 0 y 3
        int i = Random.Range(0, enemigos.Length);
       
            GameObject.Instantiate(enemigos[i]);
       
            
       
      
 }
  /*
    public void InstanciarEnemigosConSwicht()
    {
        Object[] enemigos = { prefabLata, prefabBotella, prefabAnillas, prefabCascara};
        
        for (int y = Random.Range(0, 4); y < enemigos.Length; y = y + 1)
        {
            GameObject.Instantiate(enemigos[y]);
           
        }

      
        switch (i)
        {
            case 0:
                {
                    GameObject.Instantiate(prefabLata);
                    break;
                }
            case 1:
                {
                    GameObject.Instantiate(prefabBotella);
                    break;
                }
            case 2:
                {
                    GameObject.Instantiate(prefabAnillas);
                    break;
                }
            case 3:
                {
                    GameObject.Instantiate(prefabCascara);
                    break;
                }
            default:
                {
                    GameObject.Instantiate(prefabLata);
                    break;
                }
        }
      
    }
     */
   /*public void CuandoCapturamosEnemigoCascara()
    {
        this.puntos = this.puntos + 10;     // this.vidas++;
        GameObject.Instantiate(prefabCascara);
    }

    public void CuandoPerdemosEnemigoCascara()
    {
        
        this.vidas = this.vidas - 1;        // this.vidas--;
        GameObject.Instantiate(prefabCascara);
    }
    public void CuandoCapturamosEnemigoAnillas()
    {
        
        this.puntos = this.puntos + 10;     // this.vidas++;
        //GameObject.Instantiate(prefabAnillas);
        GameObject.Instantiate(prefabAnillas);
    }

    public void CuandoPerdemosEnemigoAnillas()
    {
        
        this.vidas = this.vidas - 1;        // this.vidas--;
        GameObject.Instantiate(prefabAnillas);
    }*/
}