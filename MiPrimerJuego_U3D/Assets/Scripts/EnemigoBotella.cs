﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemigoBotella : MonoBehaviour
{
    //float es un número decimal
    public float velocidad = 10;    //10 es el valor por defecto.
    GameObject jugador;

    // Start is called before the first frame update
    void Start()
    {
        float posInicioX = Random.Range(-10, 10);

        this.transform.position = new Vector3(posInicioX, 10, 1);

        //Buscamos un Game Object que queremos manejar qué es el padre.
        jugador = GameObject.Find("Jugador_Caballito");
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 movAbajo = velocidad * Vector3.down * Time.deltaTime;
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        //Si la posición es menor que 0.
        if (this.GetComponent<Transform>().position.y <= 0)
        {
            //recolocamos en el margen izda.
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, -1);

            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
               && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
            {
               
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigoBotella();
                 Destroy(this.gameObject);
            }
            else
            {
                
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigoBotella();
                Destroy(this.gameObject);

            }
        }



    }
}

