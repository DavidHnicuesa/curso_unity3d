﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
        //float es un número decimal
    public float velocidad = 40;    //40 es el valor por defecto.


    // Update is called once per frame
    void Update()
    {
       


        //Si se presiona la tecla flecha izda.
      if (Input.GetKey(KeyCode.LeftArrow))
        {

                // Creamos el vector demovimiento, a partir del cálculo que
                // influye la velocidad (40), el vector hacia la izda. (-1,0,0): (-40,0,0)
                // Para que se mueva -40 unid. por segundo en vez de por cada frame,
                // multiplicamos por el incremento del tiempo (aprox. 0.02 seg. 50 fps) (0.8,0,0)
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
                 // velocidad es igual a unidad dividido por segundos  y multiplicado 
                 //por uno multiplicado por segundos dividido por frame igual a unidades por frame.
                 // unid/seg x 1 x seg/frame = unidades*frame

            //Una vez que se ha claculado, aplicamos el movimiento.
            //Translada el elemento a la izda.
            this.GetComponent<Transform>().Translate(vectorMov);
            
            //Si la posición es menor que -10.
            if (this.GetComponent<Transform>().position.x <= -10)
            {
                //recolocamos en el margen izda.
                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                
                //Debug.Log("Hemos chocado a la izda.");
            }
        }

      //////////////////////////////////////////////////////
      
        //Si se presiona la tecla flecha dcha.
      if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            //Translada el elemento a la dcha.
            this.GetComponent<Transform>().Translate(vectorMov);

            //Si la posición es mayor que 10.
            if (this.GetComponent<Transform>().position.x >= 10)
            {
                //recolocamos en el margen drcha.
                this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
               //Debug.Log("Hemos chocado a la drcha.");
            }
        }

      /////////////////////////////////////////////////////
        /*
          //Si se presiona la tecla flecha arriba.
        if (Input.GetKey(KeyCode.UpArrow))
          {
              //Translada el elemento a la arriba.
              this.GetComponent<Transform>().Translate(Vector3.up);
          }

          //Si se presiona la tecla flecha abajo.
        if (Input.GetKey(KeyCode.DownArrow))
          {
              //Translada el elemento a la abajo.
              this.GetComponent<Transform>().Translate(Vector3.down);
          }
         


        for (long i = 0; i < 100000L * Random.Range(0, 20000); i++)
        {
            float j = i * j;
        }
 */
    }
}
