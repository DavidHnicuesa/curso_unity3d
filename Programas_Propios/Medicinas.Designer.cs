﻿namespace WindowsFormsApp1
{
    partial class Medicinas
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.dateTimePicker1a = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2b = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txtNum1 = new System.Windows.Forms.TextBox();
            this.txtNum2 = new System.Windows.Forms.TextBox();
            this.txtNum3 = new System.Windows.Forms.TextBox();
            this.txtNum4 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtBote4 = new System.Windows.Forms.TextBox();
            this.txtBote3 = new System.Windows.Forms.TextBox();
            this.txtBote2 = new System.Windows.Forms.TextBox();
            this.txtBote1 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtBote5 = new System.Windows.Forms.TextBox();
            this.txtNum5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.txtBote6 = new System.Windows.Forms.TextBox();
            this.txtNum6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.txtDias6 = new System.Windows.Forms.TextBox();
            this.txtDias5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDias4 = new System.Windows.Forms.TextBox();
            this.txtDias3 = new System.Windows.Forms.TextBox();
            this.txtDias2 = new System.Windows.Forms.TextBox();
            this.txtDias1 = new System.Windows.Forms.TextBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(63, 448);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 0;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // dateTimePicker1a
            // 
            this.dateTimePicker1a.Location = new System.Drawing.Point(26, 337);
            this.dateTimePicker1a.Name = "dateTimePicker1a";
            this.dateTimePicker1a.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1a.TabIndex = 1;
            // 
            // dateTimePicker2b
            // 
            this.dateTimePicker2b.Location = new System.Drawing.Point(26, 383);
            this.dateTimePicker2b.Name = "dateTimePicker2b";
            this.dateTimePicker2b.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2b.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(26, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(165, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "SALAZOPIRINA 500 mg";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(362, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(362, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "0";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(26, 93);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(165, 20);
            this.textBox2.TabIndex = 7;
            this.textBox2.Text = "DOLQUINE 200mg";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(362, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "0";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(26, 139);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(165, 20);
            this.textBox3.TabIndex = 11;
            this.textBox3.Text = "TEPAZEPAN ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(362, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(26, 183);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(165, 20);
            this.textBox4.TabIndex = 15;
            this.textBox4.Text = "OMEPRAZOL 20mg grande";
            // 
            // txtNum1
            // 
            this.txtNum1.Location = new System.Drawing.Point(197, 44);
            this.txtNum1.Name = "txtNum1";
            this.txtNum1.Size = new System.Drawing.Size(29, 20);
            this.txtNum1.TabIndex = 17;
            this.txtNum1.Text = "1";
            // 
            // txtNum2
            // 
            this.txtNum2.Location = new System.Drawing.Point(197, 92);
            this.txtNum2.Name = "txtNum2";
            this.txtNum2.Size = new System.Drawing.Size(29, 20);
            this.txtNum2.TabIndex = 18;
            this.txtNum2.Text = "1";
            // 
            // txtNum3
            // 
            this.txtNum3.Location = new System.Drawing.Point(197, 138);
            this.txtNum3.Name = "txtNum3";
            this.txtNum3.Size = new System.Drawing.Size(29, 20);
            this.txtNum3.TabIndex = 19;
            this.txtNum3.Text = "1";
            // 
            // txtNum4
            // 
            this.txtNum4.Location = new System.Drawing.Point(197, 182);
            this.txtNum4.Name = "txtNum4";
            this.txtNum4.Size = new System.Drawing.Size(29, 20);
            this.txtNum4.TabIndex = 20;
            this.txtNum4.Text = "1";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(26, 25);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(55, 13);
            this.label51.TabIndex = 21;
            this.label51.Text = "Medicinas";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(193, 24);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(39, 13);
            this.label62.TabIndex = 22;
            this.label62.Text = "Diarias";
            // 
            // txtBote4
            // 
            this.txtBote4.Location = new System.Drawing.Point(297, 182);
            this.txtBote4.Name = "txtBote4";
            this.txtBote4.Size = new System.Drawing.Size(29, 20);
            this.txtBote4.TabIndex = 26;
            this.txtBote4.Text = "50";
            // 
            // txtBote3
            // 
            this.txtBote3.Location = new System.Drawing.Point(297, 138);
            this.txtBote3.Name = "txtBote3";
            this.txtBote3.Size = new System.Drawing.Size(29, 20);
            this.txtBote3.TabIndex = 25;
            this.txtBote3.Text = "30";
            // 
            // txtBote2
            // 
            this.txtBote2.Location = new System.Drawing.Point(297, 92);
            this.txtBote2.Name = "txtBote2";
            this.txtBote2.Size = new System.Drawing.Size(29, 20);
            this.txtBote2.TabIndex = 24;
            this.txtBote2.Text = "30";
            // 
            // txtBote1
            // 
            this.txtBote1.Location = new System.Drawing.Point(297, 44);
            this.txtBote1.Name = "txtBote1";
            this.txtBote1.Size = new System.Drawing.Size(29, 20);
            this.txtBote1.TabIndex = 23;
            this.txtBote1.Text = "50";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(292, 24);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(45, 13);
            this.label73.TabIndex = 27;
            this.label73.Text = "Pastillas";
            // 
            // txtBote5
            // 
            this.txtBote5.Location = new System.Drawing.Point(297, 232);
            this.txtBote5.Name = "txtBote5";
            this.txtBote5.Size = new System.Drawing.Size(29, 20);
            this.txtBote5.TabIndex = 33;
            this.txtBote5.Text = "10";
            // 
            // txtNum5
            // 
            this.txtNum5.Location = new System.Drawing.Point(197, 232);
            this.txtNum5.Name = "txtNum5";
            this.txtNum5.Size = new System.Drawing.Size(29, 20);
            this.txtNum5.TabIndex = 32;
            this.txtNum5.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(362, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "0";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(26, 233);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(165, 20);
            this.textBox11.TabIndex = 30;
            this.textBox11.Text = "TRANSTEC 35 microgramos/hora";
            // 
            // txtBote6
            // 
            this.txtBote6.Location = new System.Drawing.Point(297, 278);
            this.txtBote6.Name = "txtBote6";
            this.txtBote6.Size = new System.Drawing.Size(29, 20);
            this.txtBote6.TabIndex = 39;
            this.txtBote6.Text = "10";
            // 
            // txtNum6
            // 
            this.txtNum6.Location = new System.Drawing.Point(197, 278);
            this.txtNum6.Name = "txtNum6";
            this.txtNum6.Size = new System.Drawing.Size(29, 20);
            this.txtNum6.TabIndex = 38;
            this.txtNum6.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(362, 282);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "0";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(26, 279);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(165, 20);
            this.textBox14.TabIndex = 36;
            this.textBox14.Text = "HIDROFEROL 0.266mg";
            // 
            // txtDias6
            // 
            this.txtDias6.Location = new System.Drawing.Point(245, 278);
            this.txtDias6.Name = "txtDias6";
            this.txtDias6.Size = new System.Drawing.Size(29, 20);
            this.txtDias6.TabIndex = 46;
            this.txtDias6.Text = "15";
            // 
            // txtDias5
            // 
            this.txtDias5.Location = new System.Drawing.Point(245, 232);
            this.txtDias5.Name = "txtDias5";
            this.txtDias5.Size = new System.Drawing.Size(29, 20);
            this.txtDias5.TabIndex = 45;
            this.txtDias5.Text = "3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(242, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Días";
            // 
            // txtDias4
            // 
            this.txtDias4.Location = new System.Drawing.Point(245, 182);
            this.txtDias4.Name = "txtDias4";
            this.txtDias4.Size = new System.Drawing.Size(29, 20);
            this.txtDias4.TabIndex = 43;
            this.txtDias4.Text = "1";
            // 
            // txtDias3
            // 
            this.txtDias3.Location = new System.Drawing.Point(245, 138);
            this.txtDias3.Name = "txtDias3";
            this.txtDias3.Size = new System.Drawing.Size(29, 20);
            this.txtDias3.TabIndex = 42;
            this.txtDias3.Text = "1";
            // 
            // txtDias2
            // 
            this.txtDias2.Location = new System.Drawing.Point(245, 92);
            this.txtDias2.Name = "txtDias2";
            this.txtDias2.Size = new System.Drawing.Size(29, 20);
            this.txtDias2.TabIndex = 41;
            this.txtDias2.Text = "1";
            // 
            // txtDias1
            // 
            this.txtDias1.Location = new System.Drawing.Point(245, 44);
            this.txtDias1.Name = "txtDias1";
            this.txtDias1.Size = new System.Drawing.Size(29, 20);
            this.txtDias1.TabIndex = 40;
            this.txtDias1.Text = "1";
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(281, 448);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 47;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 318);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "Fecha Inicio";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 367);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 49;
            this.label9.Text = "Fecha Fin";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(347, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Cajas";
            // 
            // Medicinas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 494);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.txtDias6);
            this.Controls.Add(this.txtDias5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDias4);
            this.Controls.Add(this.txtDias3);
            this.Controls.Add(this.txtDias2);
            this.Controls.Add(this.txtDias1);
            this.Controls.Add(this.txtBote6);
            this.Controls.Add(this.txtNum6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.txtBote5);
            this.Controls.Add(this.txtNum5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.txtBote4);
            this.Controls.Add(this.txtBote3);
            this.Controls.Add(this.txtBote2);
            this.Controls.Add(this.txtBote1);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.txtNum4);
            this.Controls.Add(this.txtNum3);
            this.Controls.Add(this.txtNum2);
            this.Controls.Add(this.txtNum1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateTimePicker2b);
            this.Controls.Add(this.dateTimePicker1a);
            this.Controls.Add(this.btnCalcular);
            this.Name = "Medicinas";
            this.Text = "Medicinas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.DateTimePicker dateTimePicker1a;
        private System.Windows.Forms.DateTimePicker dateTimePicker2b;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox txtNum1;
        private System.Windows.Forms.TextBox txtNum2;
        private System.Windows.Forms.TextBox txtNum3;
        private System.Windows.Forms.TextBox txtNum4;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtBote4;
        private System.Windows.Forms.TextBox txtBote3;
        private System.Windows.Forms.TextBox txtBote2;
        private System.Windows.Forms.TextBox txtBote1;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox txtBote5;
        private System.Windows.Forms.TextBox txtNum5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox txtBote6;
        private System.Windows.Forms.TextBox txtNum6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox txtDias6;
        private System.Windows.Forms.TextBox txtDias5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDias4;
        private System.Windows.Forms.TextBox txtDias3;
        private System.Windows.Forms.TextBox txtDias2;
        private System.Windows.Forms.TextBox txtDias1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}

