﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Medicinas : Form
    {
        public DateTime fechaInicio;
        public DateTime fechaFinal;
        public TimeSpan dias;
        public float total = 0;
        public int dia = 0;

        public Medicinas()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            fechaInicio = dateTimePicker1a.Value.Date;
            fechaFinal = dateTimePicker2b.Value.Date;

            CuantosDias(fechaInicio, fechaFinal);
            CuantasPastillas(int.Parse(dias.Days.ToString()), int.Parse(txtNum1.Text),  int.Parse(txtBote1.Text), int.Parse(txtDias1.Text));
            label1.Text = total.ToString();
            ///
            CuantosDias(fechaInicio, fechaFinal);
            CuantasPastillas(int.Parse(dias.Days.ToString()),  int.Parse(txtNum2.Text), int.Parse(txtBote2.Text), int.Parse(txtDias2.Text));
            label2.Text = total.ToString();
            ///
            CuantosDias(fechaInicio, fechaFinal);
            CuantasPastillas(int.Parse(dias.Days.ToString()),  int.Parse(txtNum3.Text), int.Parse(txtBote3.Text), int.Parse(txtDias3.Text));
            label3.Text = total.ToString();
            ///
            CuantosDias(fechaInicio, fechaFinal);
            dia = int.Parse(dias.Days.ToString());
            CuantasPastillas(int.Parse(dias.Days.ToString()), int.Parse(txtNum4.Text), int.Parse(txtBote4.Text), int.Parse(txtDias4.Text));
            label4.Text = total.ToString();
            ///
            CuantosDias(fechaInicio, fechaFinal);
            dia = int.Parse(dias.Days.ToString());
            CuantasPastillas(int.Parse(dias.Days.ToString()),  int.Parse(txtNum5.Text), int.Parse(txtBote5.Text), int.Parse(txtDias5.Text));
            label5.Text = total.ToString();
            ///
            CuantosDias(fechaInicio, fechaFinal);
            dia = int.Parse(dias.Days.ToString());
            CuantasPastillas(int.Parse(dias.Days.ToString()),  int.Parse(txtNum6.Text), int.Parse(txtBote6.Text), int.Parse(txtDias6.Text));
            label6.Text = total.ToString();
        }

        private void CuantosDias(DateTime fechaInicio, DateTime fechaFinal)
        {
            dias = fechaFinal - fechaInicio;
        }
        private void CuantasPastillas(int dia, int pastilla, int botes, int dias)
        {
            if (dias == 1)
            {
                total = (dia * pastilla)/botes;
            }
            else
            {
                total = (dia / dias)/botes;
            }
            
            if ( total%0 != 0)
            {
                total += 1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
