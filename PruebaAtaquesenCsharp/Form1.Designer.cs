﻿namespace PruebaAtaquesenCsharp
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnAtaq10 = new System.Windows.Forms.Button();
            this.btnAtaq20 = new System.Windows.Forms.Button();
            this.btnAtaq30 = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.chkJug1 = new System.Windows.Forms.CheckBox();
            this.chkJug2 = new System.Windows.Forms.CheckBox();
            this.chkJug3 = new System.Windows.Forms.CheckBox();
            this.chkEne3 = new System.Windows.Forms.CheckBox();
            this.chkEne2 = new System.Windows.Forms.CheckBox();
            this.chkEne1 = new System.Windows.Forms.CheckBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.txtVida1 = new System.Windows.Forms.TextBox();
            this.txtVida2 = new System.Windows.Forms.TextBox();
            this.TxtVida3 = new System.Windows.Forms.TextBox();
            this.lblNombre1 = new System.Windows.Forms.Label();
            this.lblEdad1 = new System.Windows.Forms.Label();
            this.chkVip1 = new System.Windows.Forms.CheckBox();
            this.chkVip2 = new System.Windows.Forms.CheckBox();
            this.lblEdad2 = new System.Windows.Forms.Label();
            this.lblNombre2 = new System.Windows.Forms.Label();
            this.chkVip3 = new System.Windows.Forms.CheckBox();
            this.lblEdad3 = new System.Windows.Forms.Label();
            this.lblNombre3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(13, 13);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(341, 48);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Prueba de ataques";
            // 
            // btnAtaq10
            // 
            this.btnAtaq10.Location = new System.Drawing.Point(323, 84);
            this.btnAtaq10.Name = "btnAtaq10";
            this.btnAtaq10.Size = new System.Drawing.Size(79, 23);
            this.btnAtaq10.TabIndex = 4;
            this.btnAtaq10.Text = "Ataque 10";
            this.btnAtaq10.UseVisualStyleBackColor = true;
            this.btnAtaq10.Click += new System.EventHandler(this.btnAtaq10_Click);
            // 
            // btnAtaq20
            // 
            this.btnAtaq20.Location = new System.Drawing.Point(323, 127);
            this.btnAtaq20.Name = "btnAtaq20";
            this.btnAtaq20.Size = new System.Drawing.Size(78, 23);
            this.btnAtaq20.TabIndex = 5;
            this.btnAtaq20.Text = "Ataque 20";
            this.btnAtaq20.UseVisualStyleBackColor = true;
            // 
            // btnAtaq30
            // 
            this.btnAtaq30.Location = new System.Drawing.Point(323, 173);
            this.btnAtaq30.Name = "btnAtaq30";
            this.btnAtaq30.Size = new System.Drawing.Size(78, 23);
            this.btnAtaq30.TabIndex = 6;
            this.btnAtaq30.Text = "Ataque 30";
            this.btnAtaq30.UseVisualStyleBackColor = true;
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(28, 226);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(566, 20);
            this.txtResultado.TabIndex = 10;
            // 
            // chkJug1
            // 
            this.chkJug1.AutoSize = true;
            this.chkJug1.Location = new System.Drawing.Point(28, 87);
            this.chkJug1.Name = "chkJug1";
            this.chkJug1.Size = new System.Drawing.Size(73, 17);
            this.chkJug1.TabIndex = 11;
            this.chkJug1.Text = "Jugador 1";
            this.chkJug1.UseVisualStyleBackColor = true;
            // 
            // chkJug2
            // 
            this.chkJug2.AutoSize = true;
            this.chkJug2.Location = new System.Drawing.Point(28, 130);
            this.chkJug2.Name = "chkJug2";
            this.chkJug2.Size = new System.Drawing.Size(70, 17);
            this.chkJug2.TabIndex = 12;
            this.chkJug2.Text = "Jugado 2";
            this.chkJug2.UseVisualStyleBackColor = true;
            // 
            // chkJug3
            // 
            this.chkJug3.AutoSize = true;
            this.chkJug3.Location = new System.Drawing.Point(28, 176);
            this.chkJug3.Name = "chkJug3";
            this.chkJug3.Size = new System.Drawing.Size(73, 17);
            this.chkJug3.TabIndex = 13;
            this.chkJug3.Text = "Jugador 3";
            this.chkJug3.UseVisualStyleBackColor = true;
            // 
            // chkEne3
            // 
            this.chkEne3.AutoSize = true;
            this.chkEne3.Location = new System.Drawing.Point(434, 176);
            this.chkEne3.Name = "chkEne3";
            this.chkEne3.Size = new System.Drawing.Size(76, 17);
            this.chkEne3.TabIndex = 16;
            this.chkEne3.Text = "Enemigo 3";
            this.chkEne3.UseVisualStyleBackColor = true;
            // 
            // chkEne2
            // 
            this.chkEne2.AutoSize = true;
            this.chkEne2.Location = new System.Drawing.Point(434, 130);
            this.chkEne2.Name = "chkEne2";
            this.chkEne2.Size = new System.Drawing.Size(76, 17);
            this.chkEne2.TabIndex = 15;
            this.chkEne2.Text = "Enemigo 2";
            this.chkEne2.UseVisualStyleBackColor = true;
            // 
            // chkEne1
            // 
            this.chkEne1.AutoSize = true;
            this.chkEne1.Location = new System.Drawing.Point(434, 87);
            this.chkEne1.Name = "chkEne1";
            this.chkEne1.Size = new System.Drawing.Size(76, 17);
            this.chkEne1.TabIndex = 14;
            this.chkEne1.Text = "Enemigo 1";
            this.chkEne1.UseVisualStyleBackColor = true;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(186, 521);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 17;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // txtVida1
            // 
            this.txtVida1.Location = new System.Drawing.Point(527, 85);
            this.txtVida1.Name = "txtVida1";
            this.txtVida1.Size = new System.Drawing.Size(67, 20);
            this.txtVida1.TabIndex = 18;
            // 
            // txtVida2
            // 
            this.txtVida2.Location = new System.Drawing.Point(527, 129);
            this.txtVida2.Name = "txtVida2";
            this.txtVida2.Size = new System.Drawing.Size(67, 20);
            this.txtVida2.TabIndex = 19;
            // 
            // TxtVida3
            // 
            this.TxtVida3.Location = new System.Drawing.Point(527, 175);
            this.TxtVida3.Name = "TxtVida3";
            this.TxtVida3.Size = new System.Drawing.Size(67, 20);
            this.TxtVida3.TabIndex = 20;
            // 
            // lblNombre1
            // 
            this.lblNombre1.AutoSize = true;
            this.lblNombre1.Location = new System.Drawing.Point(107, 89);
            this.lblNombre1.Name = "lblNombre1";
            this.lblNombre1.Size = new System.Drawing.Size(50, 13);
            this.lblNombre1.TabIndex = 21;
            this.lblNombre1.Text = "Nombre1";
            // 
            // lblEdad1
            // 
            this.lblEdad1.AutoSize = true;
            this.lblEdad1.Location = new System.Drawing.Point(183, 88);
            this.lblEdad1.Name = "lblEdad1";
            this.lblEdad1.Size = new System.Drawing.Size(37, 13);
            this.lblEdad1.TabIndex = 22;
            this.lblEdad1.Text = "edad1";
            // 
            // chkVip1
            // 
            this.chkVip1.AutoSize = true;
            this.chkVip1.Location = new System.Drawing.Point(245, 87);
            this.chkVip1.Name = "chkVip1";
            this.chkVip1.Size = new System.Drawing.Size(41, 17);
            this.chkVip1.TabIndex = 23;
            this.chkVip1.Text = "Vip";
            this.chkVip1.UseVisualStyleBackColor = true;
            // 
            // chkVip2
            // 
            this.chkVip2.AutoSize = true;
            this.chkVip2.Location = new System.Drawing.Point(245, 129);
            this.chkVip2.Name = "chkVip2";
            this.chkVip2.Size = new System.Drawing.Size(41, 17);
            this.chkVip2.TabIndex = 26;
            this.chkVip2.Text = "Vip";
            this.chkVip2.UseVisualStyleBackColor = true;
            // 
            // lblEdad2
            // 
            this.lblEdad2.AutoSize = true;
            this.lblEdad2.Location = new System.Drawing.Point(183, 130);
            this.lblEdad2.Name = "lblEdad2";
            this.lblEdad2.Size = new System.Drawing.Size(37, 13);
            this.lblEdad2.TabIndex = 25;
            this.lblEdad2.Text = "edad2";
            // 
            // lblNombre2
            // 
            this.lblNombre2.AutoSize = true;
            this.lblNombre2.Location = new System.Drawing.Point(107, 131);
            this.lblNombre2.Name = "lblNombre2";
            this.lblNombre2.Size = new System.Drawing.Size(50, 13);
            this.lblNombre2.TabIndex = 24;
            this.lblNombre2.Text = "Nombre2";
            // 
            // chkVip3
            // 
            this.chkVip3.AutoSize = true;
            this.chkVip3.Location = new System.Drawing.Point(245, 175);
            this.chkVip3.Name = "chkVip3";
            this.chkVip3.Size = new System.Drawing.Size(41, 17);
            this.chkVip3.TabIndex = 29;
            this.chkVip3.Text = "Vip";
            this.chkVip3.UseVisualStyleBackColor = true;
            // 
            // lblEdad3
            // 
            this.lblEdad3.AutoSize = true;
            this.lblEdad3.Location = new System.Drawing.Point(183, 176);
            this.lblEdad3.Name = "lblEdad3";
            this.lblEdad3.Size = new System.Drawing.Size(37, 13);
            this.lblEdad3.TabIndex = 28;
            this.lblEdad3.Text = "edad3";
            // 
            // lblNombre3
            // 
            this.lblNombre3.AutoSize = true;
            this.lblNombre3.Location = new System.Drawing.Point(107, 177);
            this.lblNombre3.Name = "lblNombre3";
            this.lblNombre3.Size = new System.Drawing.Size(50, 13);
            this.lblNombre3.TabIndex = 27;
            this.lblNombre3.Text = "Nombre3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 556);
            this.Controls.Add(this.chkVip3);
            this.Controls.Add(this.lblEdad3);
            this.Controls.Add(this.lblNombre3);
            this.Controls.Add(this.chkVip2);
            this.Controls.Add(this.lblEdad2);
            this.Controls.Add(this.lblNombre2);
            this.Controls.Add(this.chkVip1);
            this.Controls.Add(this.lblEdad1);
            this.Controls.Add(this.lblNombre1);
            this.Controls.Add(this.TxtVida3);
            this.Controls.Add(this.txtVida2);
            this.Controls.Add(this.txtVida1);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.chkEne3);
            this.Controls.Add(this.chkEne2);
            this.Controls.Add(this.chkEne1);
            this.Controls.Add(this.chkJug3);
            this.Controls.Add(this.chkJug2);
            this.Controls.Add(this.chkJug1);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.btnAtaq30);
            this.Controls.Add(this.btnAtaq20);
            this.Controls.Add(this.btnAtaq10);
            this.Controls.Add(this.lblTitulo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnAtaq10;
        private System.Windows.Forms.Button btnAtaq20;
        private System.Windows.Forms.Button btnAtaq30;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.CheckBox chkJug1;
        private System.Windows.Forms.CheckBox chkJug2;
        private System.Windows.Forms.CheckBox chkJug3;
        private System.Windows.Forms.CheckBox chkEne3;
        private System.Windows.Forms.CheckBox chkEne2;
        private System.Windows.Forms.CheckBox chkEne1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.TextBox txtVida1;
        private System.Windows.Forms.TextBox txtVida2;
        private System.Windows.Forms.TextBox TxtVida3;
        private System.Windows.Forms.Label lblNombre1;
        private System.Windows.Forms.Label lblEdad1;
        private System.Windows.Forms.CheckBox chkVip1;
        private System.Windows.Forms.CheckBox chkVip2;
        private System.Windows.Forms.Label lblEdad2;
        private System.Windows.Forms.Label lblNombre2;
        private System.Windows.Forms.CheckBox chkVip3;
        private System.Windows.Forms.Label lblEdad3;
        private System.Windows.Forms.Label lblNombre3;
    }
}

