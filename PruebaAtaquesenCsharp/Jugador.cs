﻿using System;

public class Jugador

{
    public string nombre;
    public int edad;
    public bool haPagado;
    public float vida;
    private string ataque;

    //Constructor para inicializar las variables
	public Jugador(string n, int edad, bool haPagado)
	{
        this.nombre = n;
        this.edad = edad;
        this.haPagado = haPagado;
        this.vida = 100;
    }

    public void Rellenar()
    {

    }

    public void CargarJugador()
    {

    }

}
