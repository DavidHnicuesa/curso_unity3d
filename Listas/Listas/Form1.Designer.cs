﻿namespace Listas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblListas = new System.Windows.Forms.Label();
            this.lblListaInicial = new System.Windows.Forms.Label();
            this.txtLista1Dato1 = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.txtLista1Dato2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblListas
            // 
            this.lblListas.AutoSize = true;
            this.lblListas.Font = new System.Drawing.Font("Impact", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListas.Location = new System.Drawing.Point(198, 22);
            this.lblListas.Name = "lblListas";
            this.lblListas.Size = new System.Drawing.Size(172, 36);
            this.lblListas.TabIndex = 0;
            this.lblListas.Text = "Listas en C#";
            // 
            // lblListaInicial
            // 
            this.lblListaInicial.AutoSize = true;
            this.lblListaInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListaInicial.Location = new System.Drawing.Point(48, 69);
            this.lblListaInicial.Name = "lblListaInicial";
            this.lblListaInicial.Size = new System.Drawing.Size(120, 26);
            this.lblListaInicial.TabIndex = 1;
            this.lblListaInicial.Text = "Lista Inicial";
            // 
            // txtLista1Dato1
            // 
            this.txtLista1Dato1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLista1Dato1.Location = new System.Drawing.Point(53, 113);
            this.txtLista1Dato1.Name = "txtLista1Dato1";
            this.txtLista1Dato1.Size = new System.Drawing.Size(560, 32);
            this.txtLista1Dato1.TabIndex = 2;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(53, 185);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 3;
            this.btn1.Text = "Ordenar";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // txtLista1Dato2
            // 
            this.txtLista1Dato2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLista1Dato2.Location = new System.Drawing.Point(53, 293);
            this.txtLista1Dato2.Name = "txtLista1Dato2";
            this.txtLista1Dato2.Size = new System.Drawing.Size(560, 32);
            this.txtLista1Dato2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lista Ordenada";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 450);
            this.Controls.Add(this.txtLista1Dato2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.txtLista1Dato1);
            this.Controls.Add(this.lblListaInicial);
            this.Controls.Add(this.lblListas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblListas;
        private System.Windows.Forms.Label lblListaInicial;
        private System.Windows.Forms.TextBox txtLista1Dato1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.TextBox txtLista1Dato2;
        private System.Windows.Forms.Label label1;
    }
}

