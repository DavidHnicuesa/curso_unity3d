﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI : MonoBehaviour
{
    public List<Text> numeros;
    List<int> listaNum;
    private List<int> listaNum2;
    int menorDeTodos;

    public UnityEngine.Vector2 posInicial;
    const int ANCHO_TEXTO = 60;

    // Start is called before the first frame update
    void Start()
    {
        listaNum2 = new List<int>();
        listaNum = new List<int>();

        foreach (Text txtNum in numeros)
        {
            int num = Int32.Parse(txtNum.text);
            listaNum.Add(num);
        }
        // Es lo mismo que:
        for (int i = 0; i < numeros.Count; i++)
        {
            Text txtNum = numeros[i];
            // int num = Int32.Parse(txtNum.text); ....
        }
        menorDeTodos = OrdenarComoHumano.Menor(listaNum);
        // menorDeTodos = OrdenarComoHumano.Menor(listaNum2);
        OrdenarComoHumano.MostrarLista(listaNum);
        ColocarSeguidos();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ColocarSeguidos()
    {
        this.posInicial = this.numeros[0].GetComponent<RectTransform>().localPosition;
        /* Vector2 nuevaPos = new Vector2(this.posInicial.x + ANCHO_TEXTO, this.posInicial.y);
         this.numeros[1].GetComponent<RectTransform>().localPosition = nuevaPos;

         Vector2 nuevaPos1 = new Vector2(this.posInicial.x + ANCHO_TEXTO, this.posInicial.y);
         this.numeros[2].GetComponent<RectTransform>().localPosition = nuevaPos1;

         Vector2 nuevaPos2 = new Vector2(nuevaPos1.x + ANCHO_TEXTO, this.posInicial.y);
         this.numeros[3].GetComponent<RectTransform>().localPosition = nuevaPos2;

         Vector2 nuevaPos3 = new Vector2(nuevaPos2.x + ANCHO_TEXTO, this.posInicial.y);
         this.numeros[4].GetComponent<RectTransform>().localPosition = nuevaPos3;

         Vector2 nuevaPos4 = new Vector2(nuevaPos3.x + ANCHO_TEXTO, this.posInicial.y);
         this.numeros[5].GetComponent<RectTransform>().localPosition = nuevaPos4;
         */
        float anchoAux = this.numeros[0].rectTransform.rect.width;
        

        for (int i = 1; i< numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x + (anchoAux/2) + (this.numeros[i - 1].rectTransform.rect.width/2);
            float nuevaPosY = this.posInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
            //anchoAux = anchoAux/2 + this.numeros[i - 1].rectTransform.rect.width/2;
        }


    }
  
}
