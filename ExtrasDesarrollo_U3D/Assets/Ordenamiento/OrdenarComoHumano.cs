﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class OrdenarComoHumano
{
    // Controlar qué entra y qué sale en las funciones

	public static void Main()
    {
        int j = 3, k = 2;
        string total = "Hola, estoy sin cambiar";
        Sumar(j, k, ref total);
        Debug.Log( total);
        // return;

        List<int> numeros = new List<int>();
		numeros.Add(4);		// numeros[0] = 4
		numeros.Add(5);
		numeros.Add(3);
		numeros.Add(1);
		numeros.Add(2);
        Debug.Log("¿LISTA? " + numeros.ToString() );
		MostrarLista(numeros);
        Menor(numeros);

        Debug.Log("El menor de todos: " + Menor(numeros));
        List<int> numOrde = new List<int>();
        // Invocar al método ordenar y mostrar
        Ordenar(numeros);
        MostrarLista(numOrde);
    }
    static void Ordenar(List<int> lista)
    {
        //TODO: Ordenar, buscando el más pequeño, sacándolo y poniendo la lista ordenada
        // Después, después ordenar, llamar a MostrarLista para comprobar que OK
        // Al final devolver de alguna manera, la lista ordenada, para usarla
        // en el main
    }
    static void OrdenarBurbuja(List<int> lista)
    {
        //TODO: Ordenar intercambiando valores 
    }
    static void Sumar(int x, int y, ref string sumaTotal)
    {
        sumaTotal = "Suma " + (x + y);
    }
    public static void MostrarLista(List<int> lista)
    {
		Debug.Log("Lista: ");
		for (int i = 0; i < lista.Count; i++)
        {
            Debug.Log(lista[i] + ", ");	// numeros[3]
        }
        Debug.Log("  FIN");
    }
    //TODO: Para usar la función estática, falta algo
    public static int Menor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        { 
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
            Debug.Log("Número menor: " + menor);
        }
        return menor;
    }
    public static int maxElemeton = 100;
}
