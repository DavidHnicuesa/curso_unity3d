﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cambio_Imagen : MonoBehaviour
{
    public Image UITransform;
    // Start is called before the first frame update
    void Start()
    {
        UITransform = GameObject.Find("Imagen1").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("q"))
        {
            UITransform.sprite = Resources.Load<Sprite>("Sprites/Imagen1");
        }
        if (Input.GetKeyDown("w"))
        {
            UITransform.sprite = Resources.Load<Sprite>("Sprites/Imagen2");
        }
    }
}
